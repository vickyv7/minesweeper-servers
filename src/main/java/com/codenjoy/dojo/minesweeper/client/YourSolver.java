package com.codenjoy.dojo.minesweeper.client;

import com.codenjoy.dojo.minesweeper.client.YourDirectionSolver;
import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.RandomDice;

public class YourSolver extends YourDirectionSolver implements Solver<Board> {

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        super(dice);
    }
    
    @Override
    public String get(Board board) {
        return super.get(new BoardAdapter(board));
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                "http://10.245.129.28:8080/codenjoy-contest/board/player/uslzsujcx0g3j560wexp?code=7883044634428495750",
                new YourSolver(new RandomDice()),
                new Board());
    }

}
